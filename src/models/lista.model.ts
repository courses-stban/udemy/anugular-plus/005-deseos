import { ListaItem } from './lista-item.model';

export class Lista {
  public id: number;
  public creadaEn: Date;
  public terminadaEn: Date;
  public terminada: boolean;
  public items: ListaItem[];

  constructor(
    public titulo: string
  ) {
    this.id = new Date().getTime();
    this.creadaEn = new Date();
    this.terminada = false;
    this.items = []
  }
}
