import { Injectable } from "@angular/core";
import { Lista } from '../models';

@Injectable()
export class DeseosService {
  listas: Lista[] = [];

  constructor() {
    this.cargarStorage();
  }

  agregarLista( lista: Lista ) {
    this.listas.push(lista);
    this.guardarStorage();
  }

  borrarLista(id: number) {
    this.listas = this.listas.filter(listaData => {
      return listaData.id !== id;
    })
    this.guardarStorage();
  }

  guardarStorage() {
    localStorage.setItem( 'data', JSON.stringify(this.listas) );
  }

  cargarStorage() {
    if (localStorage.getItem('data')) {
      this.listas = JSON.parse(localStorage.getItem('data'));
    }
  }
}
